How to set up the Module

1.) Cosy the module to the modules directory

2.) Activate the module on the admin/build/modules page

3.) While logged into the website as an administrator, select Administer  Site Configuration  Democracy in Action push module settings from the menu along the left side of the site. Once the page loads, enter the Democracy in Action node address in the field labeled “Democracy in Action base URL.” Enter the Democracy in Action username in the field labeled “Democracy in Action Username.” Enter the password currently used by the module in the field labeled “Democracy in Action Current Password”. If this is the first time setting up the module, leave this field blank. Enter the new password to use in the “Democracy in Action New Password” and “Democracy in Action New Password (confirm)” fields. Click the “Save configuration” button at the bottom of the form to save the new settings. Once this is done, the module will verify that it can log into Democracy with the given credentials, and display a success message if it successfully updated its stored login information.
