<?php

/**
 * CiviCRM/Drupal module for pushing contact changes to Democracy in Action
 *
 * @author David Heiniluoma <david.heiniluoma@fmr.com>
 */

/**
 * Display help and module information
 * @param path which path of the site we're displaying help
 * @param arg array that holds the current path as would be returned from arg() function
 * @return help text for the path
 */
function diapush_help($path, $arg) {
  $output = '';  //declare output variable
  switch ($path) {
    case "admin/help#diapush":
      $output = '<p>'.  t("Copies contact changes to Democracy in Action") .'</p>';
      break;
  }
  return $output;
}

/**
 * Validate the configuration form
 * @param form the form array of the executed form
 * @param form_state the state of the form
 */
function diapush_admin_form_validate($form, &$form_state) {
  //Check that the correct current password was entered
  if ($form_state['values']['diapush_dia_current_password'] != variable_get('diapush_dia_password', '')) {
    form_set_error('', t('You did not enter the correct current Democracy in Action password.'));
  }
  //Check that the password and the password confirmation fields match
  if ($form_state['values']['diapush_dia_password'] != $form_state['values']['diapush_dia_password_confirm']) {
    form_set_error('', t('The new password does not match the confirmation field.'));
  }
  //Remove any trailing slashes from the URL
  $form_state['values']['diapush_dia_url'] = rtrim($form_state['values']['diapush_dia_url'], "/");
  //Validate the DiA login
    //Initialize CURL connection
    $ch = curl_init();
    //Set basic connection parameters:
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); //Return a string instead of printing output
    curl_setopt($ch, CURLOPT_TIMEOUT, 100);    
    //If any XML returned is not valid, suppress the errors rather than throwing an exception
    libxml_use_internal_errors(true);
    //Authenticate to DiA server
    $authURL = $form_state['values']['diapush_dia_url']
                .'/api/authenticate.sjs?email='.
                urlencode($form_state['values']['diapush_dia_username'])
                .'&password='.
                urlencode($form_state['values']['diapush_dia_password']);
    curl_setopt($ch, CURLOPT_URL, $authURL);
    $auth = curl_exec($ch);
    //Check the result of the authorization attempt. If it failed, fail validation of the form
    $responseXml = new SimpleXMLElement($auth);
    if (!$responseXml || stripos($responseXml->message, 'Successful Login') === FALSE) {
      form_set_error('', t('Could not authenticate to Democracy in Action with the given credentials.'));
    }
}

/**
 * Create the form for changing settings for this plugin
 */
function diapush_admin_form() {
  $form = array();
  $form['diapush_dia_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Democracy in Action base URL'),
    '#default_value' => variable_get('diapush_dia_url', ''),
    '#description' => t("The base URL for the Democracy in Action API. Enter without the trailing slash. Example: https://sandbox.salsalabs.com"),
    '#required' => TRUE,
  );
  $form['diapush_dia_username'] = array(
    '#type' => 'textfield',
    '#title' => t('Democracy in Action Username'),
    '#default_value' => variable_get('diapush_dia_username', ''),
    '#description' => t("The username used to log into Democracy in Action"),
    '#required' => TRUE,
  );
  $form['diapush_dia_current_password'] = array(
    '#type' => 'password',
    '#title' => t('Democracy in Action Current Password'),
    '#default_value' => '',
    '#description' => t("The current password used to log into Democracy in Action"),
  );
  $form['diapush_dia_password'] = array(
    '#type' => 'password',
    '#title' => t('Democracy in Action New Password'),
    '#default_value' => '',
    '#description' => t("The new password used to log into Democracy in Action"),
    '#required' => TRUE,
  );
  $form['diapush_dia_password_confirm'] = array(
    '#type' => 'password',
    '#title' => t('Democracy in Action New Password (confirm)'),
    '#default_value' => '',
    '#description' => t("The new password used to log into Democracy in Action (confirm)"),
    '#required' => TRUE,
  );
  return system_settings_form($form);
}

/**
 * Map the URL for changing options to the form
 */
function diapush_menu() {
  $items = array();
  $items['admin/settings/diapush'] = array(
    'title' => t('Democracy in Action push module settings'),
    'description' => t('Settings for pushing changes to Democracy in Action'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('diapush_admin_form'),
    'access arguments' => array('access administration pages'),
    'type' => MENU_NORMAL_ITEM,
  );
  return $items;
}

/**
 * Hook to be fired before database changes
 * Store the email address for the contact being changed in case the email is modified or the contact deleted (since the email address is used as a UID when syncing with Democracy in Action)
 * @param op the operation being performed
 * @param objectName the type of record being edited
 * @param objectId the key of the record being edited
 * @param objectRef the fields of the object being edited
 */
function diapush_civicrm_pre( $op, $objectName, $objectId, &$objectRef ) {
// only interested in edits or deletes to an 'individual' object
  if ( $objectName != 'Individual' || ($op != 'edit' && $op != 'delete')) {
    return;
  }
  //Store the email address for this user, in case it changes
  $old_email = diapush_get_primary_email($objectId);
  $_SESSION['diapush_old_email_address'] = $old_email;
}

/**
 * Hook to be fired after database commits. Propagates the change to Democracy in Action
 *
 * @param op the operation being performed
 * @param objectName the type of record being edited
 * @param objectId the key of the record being edited
 * @param objectRef the fields of the object being edited
 */
function diapush_civicrm_post($op, $objectName, $objectId, &$objectRef ) {
  //Only interested in changes to individuals, so stop if object is not an individual
  if ( $objectName != 'Individual') {
    return;
  }
  //Get the new email address, so we can quit now if the old and new emails are both blank
  $new_email = diapush_get_primary_email($objectId);
  //If old and new emails are both empty, display a message and don't do any synchronization with DiA
  if (drupal_strlen(trim($_SESSION['diapush_old_email_address'])) == 0 && 
    drupal_strlen(trim($new_email)) == 0) {
    //diapush_log("Operation on contact with no email address. No DiA action taken. Contact ID $objectId",WATCHDOG_INFO);
    diapush_display("Did not sync contact to Democracy in Action because there was no email address for the contact.");
    unset($_SESSION['diapush_old_email_address']);
    return;
  }
  //Get the Drupal variables for username, password and server URL
  $username = variable_get('diapush_dia_username', '');
  $password = variable_get('diapush_dia_password', '');
  $server   = variable_get('diapush_dia_url', '');
  //Initialize CURL connection
  $ch = curl_init();
  //Set basic connection parameters:
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); //Return a string instead of printing output
  curl_setopt($ch, CURLOPT_TIMEOUT, 100);
  //Set parameter to maintain cookies.
  curl_setopt($ch, CURLOPT_COOKIEFILE, '/dev/null');
  //If any XML returned is not valid, suppress the errors rather than throwing an exception
  libxml_use_internal_errors(true);
  //Authenticate to DiA server
  $authURL = $server .'/api/authenticate.sjs?email='.
            urlencode($username)
            .'&password='.
            urlencode($password);
  curl_setopt($ch, CURLOPT_URL, $authURL);
  $auth = curl_exec($ch);
  //diapush_log('DIA authorization requested from '.$authURL.'. DIA response:'.$auth);
  //Log the result of the authorization attempt
  $responseXml = new SimpleXMLElement($auth);
  if (stripos($responseXml->message, 'Successful Login') !== FALSE) {
    diapush_log('Successfully authenticated to Democracy in Action ',
    WATCHDOG_INFO);
  }
  else {
    diapush_log('Failed to log into Democracy in Action. Returned error was:'.
              $auth,
              WATCHDOG_ERROR);
    diapush_display("Could not authenticate to Democracy in Action. Please confirm the login details in the administration section of Drupal.", 'error');
    unset($_SESSION['diapush_old_email_address']);
    return;
  }
  //Gather the contact details to be sent to DIA
  $fname = CRM_Core_DAO::getFieldValue('CRM_Contact_DAO_Contact', $objectId, 'first_name');
  $lname = CRM_Core_DAO::getFieldValue('CRM_Contact_DAO_Contact', $objectId, 'last_name');
  $title = CRM_Core_DAO::getFieldValue('CRM_Contact_DAO_Contact', $objectId, 'job_title');
  $org = CRM_Core_DAO::getFieldValue('CRM_Contact_DAO_Contact', $objectId, 'organization_name');
  $address_array = diapush_get_primary_address($objectId);
  $street = $address_array['street_address'];
  $street2 = $address_array['supplemental_address_1'];
  $city = $address_array['city'];
  $state = $address_array['state'];
  $zip = $address_array['postal_code'];
  //Put the data fields into an array
  $fields = array('object' => 'supporter', 
                  'First_Name' => $fname,
                  'Last_Name' => $lname,
                  'Email' => $new_email, 
                  'Organization' => $org, 
                  'Title' => $title, 
                  'Street' => $street, 
                  'Street_2' => $street2, 
                  'City' => $city, 
                  'State' => $state, 
                  'Zip' => $zip);
  //Build the string to use for the request
  $contact_params_str = '?xml';
  foreach ($fields as $key => $val) {
    $contact_params_str .= '&'. $key .'='. urlencode($val);
  }
  //Switch based on if this is a create, update or delete action
  switch ($op) {
    case "create":
      //Send save request to DIA
      $url = $server ."/save". $contact_params_str;
      curl_setopt($ch, CURLOPT_URL, $url);
      //diapush_log('create contact requested. submitting to '.$url);
      $out = curl_exec($ch);
      //diapush_log('create contact requested. DIA response:'.$out);
      //Log the result of the create DiA action
      $responseXml = new SimpleXMLElement($out);
      if ($responseXml->success) {
        diapush_log('Successfully created member with email address "' .
                    $new_email .
                    '" in Democracy in Action',
                    WATCHDOG_INFO);
        diapush_display("Successfully saved member to Democracy in Action.");
        //Log the error
      }
      else {
        //If an error I understand, log just the message
        if ($responseXml->error) {
        diapush_log('Failed to create member with email address "'.
                  $new_email
                  .'" in Democracy in Action. Returned error message was: '.
                  $responseXml->error,
                  WATCHDOG_ERROR);
        diapush_display("Could not save contact to Democracy in Action. DiA error: ". $responseXml->error, 'error');
        }
        else {//If I don't understand the error, log the verbose result
          diapush_log('Failed to create member with email address "'.
                  $new_email
                  .'" in Democracy in Action. Unexpected error was: '.
                  $out,
                  WATCHDOG_ERROR);
          diapush_display("Could not save contact to Democracy in Action. Please see the log for error details.", 'error');
        }
      }
      break;
    case "edit":
      //Get the key for the supporter in Democracy in Action
      $dia_contact_key = diapush_get_dia_key_for_email($_SESSION['diapush_old_email_address'], $ch);
      $contact_params_str .= '&key='. $dia_contact_key;
      //Log a warning if the user being edited could not be found on Democracy in Action
      if ($dia_contact_key==0) {
        diapush_log('Could not find  existing member with email address "'.
                  $_SESSION['diapush_old_email_address']
                  .'" in Democracy in Action. Creating new member on DiA',
                  WATCHDOG_WARNING);
        diapush_display("Could not find existing contact with email ". $_SESSION['diapush_old_email_address'] ." in Democracy in Action, so creating a new DiA contact", 'warning');
      }
      //Update contact on DIA
      $url = $server ."/save". $contact_params_str;
      curl_setopt($ch, CURLOPT_URL, $url);
      //diapush_log('edit contact requested. submitting to '.$url);
      $out = curl_exec($ch);
      //diapush_log('edit contact requested. DIA response:'.$out);
      //Log the result of the edit DiA action
      $responseXml = new SimpleXMLElement($out);
      if ($responseXml->success) {
        diapush_log('Successfully updated member with email address "'.
                  $_SESSION['diapush_old_email_address']
                  .'" (DiA supporter # '.
                  $dia_contact_key
                  .') in Democracy in Action',
                  WATCHDOG_INFO);
        diapush_display("Successfully saved member in Democracy in Action.");
      }
      else {
        //If an error I understand, log just the message
        if ($responseXml->error) {
          diapush_log('Failed to edit member with email address "' .
                    $_SESSION['diapush_old_email_address'] .
                    '" (DiA supporter # ' .
                    $dia_contact_key .
                    ') in Democracy in Action. Returned error message was: '.
                    $responseXml->error,
                    WATCHDOG_ERROR);
          diapush_display("Could not update contact in Democracy in Action. DiA error: ". $responseXml->error, 'error');
        }
        else {  //If I don't understand the error, log the verbose result
          diapush_log('Failed to edit member with email address "' .
                    $_SESSION['diapush_old_email_address'] .
                    '" (DiA supporter # ' .
                    $dia_contact_key .
                    ') in Democracy in Action. Unexpected error was: ' .
                    $out,
                    WATCHDOG_ERROR);
          diapush_display("Could not update contact in Democracy in Action. Please see the log for error details.", 'error');
        }
      }
      break; 
    case "delete":
      //Get the key for the supporter in Democracy in Action
      $dia_contact_key = diapush_get_dia_key_for_email($_SESSION['diapush_old_email_address'], $ch);
      //Log an error if the user being deleted could not be found on Democracy in Action
      if ($dia_contact_key==0) {
        diapush_log('Could not find member to delete with email address "'.
                  $_SESSION['diapush_old_email_address']
                  .'" in Democracy in Action.',
                  WATCHDOG_ERROR);
        diapush_display("Could not find contact in Democracy in Action to delete.", 'warning');
        break;
      }
      //Delete contact on DIA
      $url = $server ."/delete?xml&object=supporter&key=$dia_contact_key";
      curl_setopt($ch, CURLOPT_URL, $url);
      //diapush_log('delete contact requested. submitting to '.$url);
      $out = curl_exec($ch);
      //diapush_log('delete contact requested. DIA response:'.$out);
      //Log the result of the DiA delete action
      $responseXml = new SimpleXMLElement($out);
      if ($responseXml->success) {
        diapush_log('Successfully deleted member with email address "'.
                    $_SESSION['diapush_old_email_address']
                    .'" (DiA supporter # '.
                    $dia_contact_key
                    .') in Democracy in Action',
                    WATCHDOG_INFO);
        diapush_display("Successfully deleted member from Democracy in Action.");
      }
      else {
        //If an error I understand, log just the message
        if ($responseXml->error) {
          diapush_log('Failed to delete member with email address "'.
                      $_SESSION['diapush_old_email_address']
                    .'" (DiA supporter # '.
                    $dia_contact_key
                    .') in Democracy in Action. Returned error message was: '.
                    $responseXml->error,
                    WATCHDOG_ERROR);
          diapush_display("Could not delete contact from Democracy in Action. DiA error: ". $responseXml->error, 'error');
        }
        else { //If I don't understand the error, log the verbose result
          diapush_log('Failed to delete member with email address "' .
                    $_SESSION['diapush_old_email_address']
                    .'" (DiA supporter # ' .
                    $dia_contact_key
                    .') in Democracy in Action. Unexpected error was: '.
                    $out,
                    WATCHDOG_ERROR);
          diapush_display("Could not delete contact from Democracy in Action. Please see the log for error details.", 'error');
        }
      }    
    break;
  }
  //Close the connection
  curl_close($ch);
  unset($_SESSION['diapush_old_email_address']);
}

/**
 * Lookup the DiA key for a given email address
 *
 * @param email the email address to look up
 * @param curl_instance an instance of curl which has been authenticated to DiA
 * @ return the key for the given email address on success, and 0 if the email was not found on DiA or the email was blank
 */
function diapush_get_dia_key_for_email($email, $curl_instance) {
  //If email is blank, return key of 0
  if (drupal_strlen(trim($email))==0) {
    return 0;
  }
  //Get key for existing contact
  $url = variable_get('diapush_dia_url', '') ."/api/getObjects.sjs?object=supporter&condition=Email=". urlencode($email);
  curl_setopt($curl_instance, CURLOPT_URL, $url);
  $xmlstr = curl_exec($curl_instance);
  //diapush_log("looking up user with email address $email in DiA, response: ".$xmlstr);
  //Parse the XML response to get the key, and add it to the params string. If no key was found, use key 0 to create a new record
  $xml = new SimpleXMLElement($xmlstr);
  //If there are results, return the key of the first supporter. If the count of supporters is 0, return 0
  return ($xml->supporter->count > 0) ? $xml->supporter->item[0]->key : 0;
}

/*
 * Get the primary email address for the given contact's ID
 *
 * @param contact_id the contact's CiviCRM ID
 * @return the primary email address for the contact, or null if no primary email was found
 */
function diapush_get_primary_email($contact_id) {
  //Get all the emails associated with the given contact ID
  $array = array();
  $all_user_emails = CRM_Core_DAO::commonRetrieveAll('CRM_Core_DAO_Email', 'contact_id', $contact_id, $array, array('is_primary', 'email'));
  //diapush_log(var_export($all_user_emails,true));
  //Look for the email with the primary variable set, and return that address
  foreach ($all_user_emails as $email) {
    if ($email['is_primary']=='1') {
      //            diapush_log('Primary email: '.$email['email']);
      return $email['email'];
    }
  }
  //    diapush_log("No primary email found for contact $contact_id");
  //If no primary address was found, return null
  return NULL;
}

/*
 * Get the primary address for the given contact's ID
 *
 * @param contact_id the contact's CiviCRM ID
 * @return the primary address for the contact, or null if no primary address was found
 */
function diapush_get_primary_address($contact_id) {
  //Get all the addresses for the given contact ID
  $array = array();
  $all_addresses = CRM_Core_DAO::commonRetrieveAll('CRM_Core_DAO_Address',
                                                    'contact_id',
                                                    $contact_id,
                                                    $array,
                                                    array('is_primary',
                                                          'street_address',
                                                          'supplemental_address_1',
                                                          'supplemental_address_2',
                                                          'city',
                                                          'state_province_id',
                                                          'postal_code'));
  //Find the primary address, and lookup the state name for it, adding it to the array
  foreach ($all_addresses as $address) {
    if ($address['is_primary']=='1') {
      $address['state'] = CRM_Core_DAO::getFieldValue('CRM_Core_DAO_StateProvince',
                                                            $address['state_province_id'],
                                                            'abbreviation',
                                                            'id');
      return $address;
    }
  }
  //diapush_log("No primary address found for contact $contact_id",WATCHDOG_INFO);
  //If no primary address was found, return null
  return NULL;
}

/*
 * Wrapper for the drupal logging function. Logs the given message
 *
 * @param msg the message to log
 * @param level the message level to log. Defaults to DEBUG
 * @param type the message type to log. Defaults to CiviCRM-diapush
 */
function diapush_log($msg, $level = WATCHDOG_DEBUG, $type = 'CiviCRM-diapush') {
  watchdog($type, htmlentities($msg), NULL, $level);
}

/*
 * Wrapper for the drupal message display function. Displays the given message if the user has admin access
 *
 * @param msg the message to display
 * @param level the message level to display with. Defaults to INFO
 */
function diapush_display($msg, $level = 'info') {
  if (user_access('access administration pages')) {
    drupal_set_message(t($msg), $level);
  }
}
